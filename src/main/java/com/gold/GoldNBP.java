package com.gold;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.time.LocalDate;
import java.util.List;

public interface GoldNBP {
    @GET("/api/cenyzlota/{startDate}/{endDate}")
    Call<List<Gold>> goldByDate(
            @Path("startDate") LocalDate startDate,
            @Path("endDate") LocalDate endDate,
            @Query("format") String json
            );


}
