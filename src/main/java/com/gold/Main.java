package com.gold;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sun.misc.IOUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws DaysException {
        final String baseUrl = "http://api.nbp.pl";
        double averge = 0;

        int days = getDaysFromUser();

        Retrofit retrofit = retrofitBuilder(baseUrl);
        GoldNBP goldNBP = retrofit.create(GoldNBP.class);

        Call<List<Gold>> call = getListCall(goldNBP, days);

        Response<List<Gold>> response;

        try {
            response = call.execute();
            averge = getAverge(response);
            String recomendation = recomendation(averge, get3DaysAverge(response));
            printGoldRespons(response, recomendation, averge);
            printToCSVFile(response, averge, recomendation);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static int getDaysFromUser() throws DaysException {
        Scanner in = new Scanner(System.in);
        System.out.println("podaj z ilu ostanich dni chcesz kurs zlota?");
        int days = in.nextInt();
        validate(days);
        return days;
    }

    private static void validate(int days) throws DaysException {
        if (days > 365) {
            throw new DaysException("Zbyt dlugi okres!");
        }
        if (days < 3) {
            throw new DaysException("Zbyt maly okres!");
        }
    }

    private static String recomendation(double averg, double daysAverge) {
        if (daysAverge / averg > 0.99 || daysAverge / averg < 1.01) {
            return "Uwaga, to nie jest porada inwestycyjna! \nTRZYMAJ!";
        }
        if (daysAverge / averg > 1.0) {
            return "Uwaga, to nie jest porada inwestycyjna! \nSPRZEDAWAJ!";
        } else {
            return "Uwaga, to nie jest porada inwestycyjna! \nKUPUJ!";
        }
    }

    private static Retrofit retrofitBuilder(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private static void printToCSVFile(Response<List<Gold>> response, double averge, String recomendation) {
        File file = new File("output.csv");

        try (Writer writer = new FileWriter(file);
             CSVPrinter printer = CSVFormat.RFC4180
                     .withHeader("Data", "Cena", "Średnia", "Rekomendacja")
                     .print(writer)) {

            for (Gold nbp : response.body()) {
                printer.printRecord(nbp.getCena(), nbp.getData(), averge, recomendation);
            }
            printer.flush();
            printer.close();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Zapis do pliku csv zakończony");
    }

    private static void printGoldRespons(Response<List<Gold>> response, String recomendation, double averge) {
        response.body().stream().map(gold -> gold.getCena() + ", " + gold.getData()).forEach(System.out::println);
        System.out.println("średnia: " + averge);
        System.out.println(recomendation);

    }

    private static Call<List<Gold>> getListCall(GoldNBP goldNBP, int days) {
        return goldNBP.goldByDate(getStartDate(LocalDate.now(), days), LocalDate.now(), "json");
    }

    private static LocalDate getStartDate(LocalDate endDate, int days) {
        return endDate.minus(days + 1, ChronoUnit.DAYS);
    }

    public static double getAverge(Response<List<Gold>> response) {
        int size = response.body().size();
        int sum = 0;
        for (int i = 0; i < size; i++) {
            sum += response.body().get(i).getCena();
        }
        return (double) sum / size;
    }

    public static double get3DaysAverge(Response<List<Gold>> response) {
        int size = response.body().size();
        int sum = 0;
        for (int i = size; i < size - 3; i--) {
            sum += response.body().get(i).getCena();
        }
        return (double) sum / size;
    }


}
