package com.gold;

public class DaysException extends Exception {
    public DaysException() {
    }

    public DaysException(String message) {
        super(message);
    }
}
